package com.mgrsys.token.sample

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
interface ApiClient {

    @GET("get/path")
    fun getResponse(): Single<Response<SomeData>>
}