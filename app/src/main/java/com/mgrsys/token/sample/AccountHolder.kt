package com.mgrsys.token.sample

import com.mgrsys.oauth.data.TokenHolder

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
open class AccountHolder : TokenHolder {

    @Volatile
    override var token = ""

    @Volatile
    override var refreshToken = ""

}