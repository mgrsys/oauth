package com.mgrsys.token.sample

import com.mgrsys.oauth.data.AuthData
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
interface AuthClient {
    @GET("path/to/updateToken")
    fun updateToken(textViewItem: Int, anotherParam: String): Single<Response<AuthData>>
}