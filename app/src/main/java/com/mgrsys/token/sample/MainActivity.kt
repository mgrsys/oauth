package com.mgrsys.token.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.mgrsys.oauth.OAuthAuthenticator
import com.mgrsys.oauth.OAuthInterceptor
import com.mgrsys.oauth.data.AuthData
import com.mgrsys.oauth.network.BaseOAuthInterceptor
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class MainActivity : AppCompatActivity() {

    val mockAuthClient = object : AuthClient {

        var refreshCount1 = AtomicInteger()
        var refreshCount2 = AtomicInteger()

        override fun updateToken(textViewItem: Int, anotherParam: String): Single<Response<AuthData>> {
            return Single.fromCallable {
                val success: Response<AuthData> = Response.success(AuthData(if (refreshCount1.get() % 2 == 0) "rotten-token" else "normal-token", "refresh-token"))

                runOnUiThread {
                    textStatusRequest1.text = "refresh token ${refreshCount1.incrementAndGet()}"
                }

                return@fromCallable success
            }.delay(1000, TimeUnit.MILLISECONDS)
        }
    }

    val accountHolder = AccountHolder()

    val authenticator = OAuthAuthenticator(accountHolder) { tokenHolder ->
        // need refresh token here
        return@OAuthAuthenticator mockAuthClient.updateToken(1, "param2")
            .map { it.body()!! }
    }

    val okHttpClient by lazy {
        OkHttpClient.Builder()
            .authenticator(authenticator)
            .addInterceptor(OAuthInterceptor(accountHolder))
            .build()
    }

    val retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl("http://localhost")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    val networkBehavior = NetworkBehavior.create().apply {
        setErrorPercent(100)
        setErrorFactory({
            Response.error<Any>(401, ResponseBody.create(null, ByteArray(0)))
        })
    }

    val mockRetrofit = MockRetrofit.Builder(retrofit)
        .networkBehavior(networkBehavior)
        .build()

    val apiDelegate = mockRetrofit.create(ApiClient::class.java)

    val apiClient = object : ApiClient {
        override fun getResponse(): Single<Response<SomeData>> {
            val response = Single.just(Response.success(ResponseBody.create(null, "")))
            return apiDelegate.returningResponse(response).getResponse()
        }
    }

    fun makeResponse(): okhttp3.Response {
        return okhttp3.Response.Builder()
            .protocol(Protocol.HTTP_2)
            .request(Request.Builder()
                .header(BaseOAuthInterceptor.ACCESS_TOKEN_HEADER, accountHolder.token)
                .url("http://localhost")
                .build())
            .code(200)
            .message("")
            .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        authorize(accountHolder)

        request1.setOnClickListener {
            apiClient.getResponse()
                .doOnSubscribe {
                    authenticator.authenticate(null, makeResponse())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    textStatusRequest1.text = "request send"
                }
                .subscribe({
                    textStatusRequest1.text = "success"
                }, {
                    handleError(it)
                    textStatusRequest1.text = "error"
                })
        }

        request2.setOnClickListener {
            apiClient.getResponse()
                .doOnSubscribe {
                    authenticator.authenticate(null, makeResponse())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    textStatusRequest2.text = "request send"
                }
                .subscribe({
                    textStatusRequest2.text = "success"
                }, {
                    handleError(it)
                    textStatusRequest2.text = "error"
                })
        }
    }

    private fun handleError(e: Throwable) {
        Log.e("tag", "error", e)
    }

    private fun authorize(accountHolder: AccountHolder) {
        accountHolder.token = "rotten-token"
        accountHolder.refreshToken = "init-refresh-token"
    }
}
