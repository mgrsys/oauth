package com.mgrsys.token.sample

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import okhttp3.OkHttpClient
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, true, false)

    lateinit var mockRetrofit: MockRetrofit
    lateinit var retrofit: Retrofit

    fun setup() {
        retrofit = Retrofit.Builder().baseUrl("http://test.com")
            .client(OkHttpClient())
//            .addConverterFactory(JacksonConverterFactory.create())
            .build()

        val behavior = NetworkBehavior.create()

        mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(behavior)
            .build()
    }

    @Test
    fun testSuccess() {
//        val delegate = mockRetrofit.create(AuthClient::class.java)
//        val successClient = object : AuthClient {
//            override fun updateToken(refreshToken: String): Single<AuthData> {
//                val response = Single.just(AuthData(
//                    token = "token",
//                    refreshToken = "refresh-token"
//                ))
//                return delegate.returningResponse(response).updateToken(refreshToken)
//            }
//        }

//        val response = successClient.updateToken("no-matter").blockingGet()
//        Assert.assertTrue(response.token == "token")
    }
}
