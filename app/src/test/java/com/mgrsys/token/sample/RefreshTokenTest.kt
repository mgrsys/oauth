package com.mgrsys.token.sample

import com.mgrsys.oauth.data.AuthData
import com.mgrsys.oauth.data.TokenHolder
import com.mgrsys.oauth.network.AuthClient
import com.mgrsys.oauth.network.BaseOAuthAuthenticator
import com.mgrsys.oauth.network.BaseOAuthInterceptor
import io.reactivex.Single
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
@RunWith(JUnit4::class)
class RefreshTokenTest {

    lateinit var tokenHolder: TokenHolder
    lateinit var mockRetrofit: MockRetrofit
    lateinit var retrofit: Retrofit


    @Before
    fun setup() {
        val tokenHolder = Mockito.mock(TokenHolder::class.java)

        val okHttpClient = OkHttpClient.Builder()
            .authenticator(BaseOAuthAuthenticator(tokenHolder))
            .addInterceptor(BaseOAuthInterceptor(tokenHolder))
            .build()

        retrofit = Retrofit.Builder().baseUrl("http://test.com")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        val behavior = NetworkBehavior.create()

        mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(behavior)
            .build()
    }

    @Test
    fun testSuccessResponse() {
        val delegate = mockRetrofit.create(AuthClient::class.java)
        val successClient = object : AuthClient {
            override fun updateToken(refreshToken: String): Single<Response<AuthData>> {
                val response = Single.just(Response.success(AuthData(
                    token = "token",
                    refreshToken = "refresh-token"
                )))
                return delegate.returningResponse(response.blockingGet()).updateToken(refreshToken)
            }
        }

        val response = successClient.updateToken("no-matter").blockingGet()
        Assert.assertTrue(response.isSuccessful)
    }

//    @Test
//    fun testSuccess() {
//        val delegate = mockRetrofit.create(AuthClient::class.java)
//        val successClient = object : AuthClient {
//            override fun updateToken(refreshToken: String): Single<Response<AuthData>> {
//                val response = Single.just(Response.success(AuthData(
//                    token = "token",
//                    refreshToken = "refresh-token"
//                )))
//                return delegate.returningResponse(response.blockingGet()).updateToken(refreshToken)
//            }
//        }
//
//        val response = successClient.updateToken("no-matter").blockingGet()
//        Assert.assertTrue(response.isSuccessful)
//    }


}