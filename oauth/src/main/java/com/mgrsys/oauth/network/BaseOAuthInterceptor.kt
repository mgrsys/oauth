package com.mgrsys.oauth.network

import com.mgrsys.oauth.data.TokenHolder
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
abstract class BaseOAuthInterceptor(val tokenHolder: TokenHolder) : Interceptor {

    companion object {
        val ACCESS_TOKEN_HEADER = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        if (chain.request().header(getTokenHeaderName()) == null) {
            requestBuilder.addHeader(getTokenHeaderName(), tokenHolder.token)
        }

        return chain.proceed(requestBuilder.build())
    }

    open fun getTokenHeaderName(): String {
        return ACCESS_TOKEN_HEADER
    }

}