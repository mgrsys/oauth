package com.mgrsys.oauth.network

import com.mgrsys.oauth.data.TokenHolder
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

/**
 * Authenticate user by calling authenticate(Route, Response) method, when rest-api return 401 error
 *
 * By synchronized modifier and blocking refreshToken(TokenHolder) method,
 * other requests for refresh token will be wait and only after that,
 * there are repeated queries with the updated token.
 * @param tokenHolder data with access- and refresh- tokens
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
abstract class BaseOAuthAuthenticator(val tokenHolder: TokenHolder) : okhttp3.Authenticator {

    @Synchronized
    override fun authenticate(route: Route?, response: Response): Request? {
        val storedToken = tokenHolder.token
        val rottenToken = response.request().header(BaseOAuthInterceptor.ACCESS_TOKEN_HEADER)

        val requestBuilder = response.request().newBuilder()

        if (isTokenRotten(storedToken, rottenToken)) {
            refreshToken(tokenHolder)
        }

        return buildRequest(requestBuilder)
    }

    protected open fun isTokenRotten(storedToken: String, rottenToken: String): Boolean {
        return storedToken == rottenToken
    }

    protected open fun getTokenHeaderName(): String {
        return BaseOAuthInterceptor.ACCESS_TOKEN_HEADER
    }

    protected open fun buildRequest(requestBuilder: Request.Builder): Request {
        return requestBuilder.header(getTokenHeaderName(), tokenHolder.token).build()
    }

    /**
     * Blocking refresh token.
     * This method should load new access and refresh token
     * After that, save it into tokenHolder
     * @param tokenHolder data with access- and refresh- tokens
     */
    abstract fun refreshToken(tokenHolder: TokenHolder)
}