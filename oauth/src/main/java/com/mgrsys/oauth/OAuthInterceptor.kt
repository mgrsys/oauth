package com.mgrsys.oauth

import com.mgrsys.oauth.data.TokenHolder
import com.mgrsys.oauth.network.BaseOAuthInterceptor

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
class OAuthInterceptor(tokenHolder: TokenHolder) : BaseOAuthInterceptor(tokenHolder) {

}