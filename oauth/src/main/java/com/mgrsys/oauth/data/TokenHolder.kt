package com.mgrsys.oauth.data

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
interface TokenHolder {
    var token: String
    var refreshToken: String
}