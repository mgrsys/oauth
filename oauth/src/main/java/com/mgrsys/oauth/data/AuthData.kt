package com.mgrsys.oauth.data

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
data class AuthData(val token: String?,
                    val refreshToken: String?)