package com.mgrsys.oauth

import com.mgrsys.oauth.data.AuthData
import com.mgrsys.oauth.data.TokenHolder
import com.mgrsys.oauth.network.BaseOAuthAuthenticator
import io.reactivex.Single

/**
 * @param tokenHolder token holder
 * @param refreshToken callback for retrieve new user auth data. It`s saves automatically
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
class OAuthAuthenticator(tokenHolder: TokenHolder,
                         private val refreshToken: (TokenHolder) -> Single<AuthData>
) : BaseOAuthAuthenticator(tokenHolder) {

    override fun refreshToken(tokenHolder: TokenHolder) {
        val data = refreshToken.invoke(tokenHolder).blockingGet()
        tokenHolder.token = data.token ?: ""
        tokenHolder.refreshToken = data.refreshToken ?: ""
    }
}