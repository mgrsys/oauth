//package com.mgrsys.oauth
//
//import junit.framework.Assert
//import okhttp3.Protocol
//import okhttp3.Response
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.junit.runners.JUnit4
//
///**
// * @since 2018
// * @author Anton Vlasov - whalemare
// */
//@RunWith(JUnit4::class)
//class AuthenticatorTest {
//
//    @Test
//    fun testMultiRequestToChangeToken() {
//        var mustBeZero = 1
//
//        val tokenHolder = object : TokenHolder {
//            override var token: String = "initial-token"
//            override var refreshToken: String = "initial-refresh-token"
//
//            override fun refresh() {
//                Thread.sleep(300)
//                token = "token-$token"
//                refreshToken = "refreshToken-$refreshToken"
//                mustBeZero--
//            }
//        }
//
//        val authenticator = BaseOAuthAuthenticator(tokenHolder)
//
//        val request = okhttp3.Request.Builder()
//            .addHeader(BaseOAuthInterceptor.ACCESS_TOKEN_HEADER, "initial-token")
//            .url("http://localhost")
//            .build()
//
//        val response = Response.Builder()
////            .header(OAuthInterceptor.ACCESS_TOKEN_HEADER, "initial-token")
//            .request(request)
//            .protocol(Protocol.HTTP_2)
//            .code(401)
//            .message("message")
//            .build()
//
//        Assert.assertTrue(mustBeZero == 0)
//    }
//}