#OAuth Authorization
Автоматическое обновление токена с учетом конкурентности

## Как использовать

* Создаем `Authenticator`. Описываем в нем метод обновления токена

```kotlin
    val authenticator = OAuthAuthenticator(accountHolder) { tokenHolder ->
        return@OAuthAuthenticator authRestApiClient.updateToken(tokenHolder.token, tokenHolder.refreshToken)
    }
```

* Создаем `OkHttpClient`, куда добавляем ему ранее созданнный `Authenticator`.
Также добавляем собственный `Interceptor` который будет добавлять токен в заголовок. 
Можно использовать предустановленный вариант `OAuthInterceptor(AccountHolder)`

```kotlin
    val okHttpClient = OkHttpClient.Builder()
            .authenticator(authenticator)
            .addInterceptor(OAuthInterceptor(accountHolder))
            .build()
```

* Скармливаем наш `okHttpClient` инстансу Retrofit

```kotlin
    val retrofit = Retrofit.Builder()
        .client(okHttpClient)
        //...
        //...
        .build()
```

* Тестируем и отписываемся о найденных багах